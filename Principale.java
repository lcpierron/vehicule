class Principale
{
    public static void main(String[] args) {
        A a = new B();
        B b = new B();

        a = b;

        a.afficheStatic();
        b.afficheStatic();

        a.afficheNom();
        b.afficheNom();

        System.out.println(a instanceof A);
        System.out.println(a instanceof B);
        System.out.println(b instanceof A);
        System.out.println(b instanceof B);

        C c1 = new A();
        C c2 = (C) new A();

        A ac = new C();
        b = (B) ac;

        ac.afficheNom();
        ac.afficheds();
        ac.afficheStatic();
        ac.afficheStatic(4);
        ((C) ac).afficheStatic(4);
        ((C) ac).afficheStatic(5.6);

        System.out.println(ac instanceof InterfaceA);
        System.out.println(ac instanceof A);
        System.out.println(ac instanceof B);
        System.out.println(ac instanceof C);

    }
}
interface InterfaceA {
    int base = 20;

    String nom();

    void afficheNom();
}

class A implements InterfaceA {
    public String nom() {
        return "A";
    }

    public void afficheNom() {
        System.out.println("affiche␣:␣" + nom());
    }

    public void afficheds() {
        System.out.println("affiche␣" + base + "␣:␣" + nom());
    }

    public static void afficheStatic() {
        System.out.println("static␣A");
    }
}

class B extends A {
    public String nom() {
        return "B";
    }

    public static void afficheStatic() {
        System.out.println("static␣B");
    }
}

class C extends A {
    public String nom() {
        return "C";
    }

    public static void afficheStatic(int unit) {
        System.out.println("affiche␣:␣" + base + unit);
    }
}

