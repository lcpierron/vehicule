
/**
 * Décrivez votre classe VTT ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class VTT extends Velo
{

    // limite d'usage avant l'entretien
    private static final int[] LIMITES_HLOCATION = {100, 500, 1000};
    private static final int[] LIMITES_HENTRETIEN = {10, 40, 20, 10};

    /**
     * Constructeur d'objets de classe VTT
     */
    public VTT()
    {
        super("VTT");
    }

    /**
     * Méthode entretienNecessaire
     *
     * @return un booléen qui indique si l'entretien est nécessaire
     */
    @Override
    public boolean entretienNecessaire()
    {
        boolean entretien = false;
        int heuresLocation = getNbHeuresLocation();
        int heuresDepuisEntretien = getNbHeuresDepuisEntretien();

        int i = 0;
        for (i=0; i < LIMITES_HLOCATION.length && heuresLocation >= LIMITES_HLOCATION[i]; i++) {
            // rien à effectuer ici, la recherche de l'indice de maintenance est fait dans l'entête
            // de la boucle
        };
        entretien = (heuresDepuisEntretien > LIMITES_HENTRETIEN[i]);

        return entretien;
    }
}
